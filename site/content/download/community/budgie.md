+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-budgie-18.1.0-stable-x86_64.iso"
Download_x64_Checksum = "158e417b05a5a1468706809a7f4b19891d31075e"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-budgie-18.1.0-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-budgie-18.1.0-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Budgie"
Screenshot = "budgie-full.jpg"
edition = "Community"
shortDescription = "For people who want a simple and elegant desktop"
Thumbnail = "budgie-full.jpg"
Version = "18.1.0"
date = "2019-09-16"
title = "Budgie"
type="download-edition"
weight = "5"
meta_description = "Manjaro budgie for people who want a simple and elegant desktop"
meta_keywords = "manjaro budgie download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Budgie, the desktop of the [Solus project](https://solus-project.com/). Budgie focuses on providing a simple-to-use and elegant desktop that fulfills the needs of a modern user.

If you are looking for older images check the [Budgie](https://osdn.net/projects/manjaro-archive/storage/mate/) archive.


