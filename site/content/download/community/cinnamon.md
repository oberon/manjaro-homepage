+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-19.0.1-200228-linux54.iso"
Download_x64_Checksum = "960f009dcbe0be31ed4f400a70c47d5fe9817c62"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-19.0.1-200228-linux54.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-19.0.1-200228-linux54.iso.torrent"
Download_Minimal_x64 = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-19.0.1-minimal-200228-linux54.iso"
Download_Minimal_x64_Checksum = "bd4571c652fcb31508d932ecbd894720d54c6b35"
Download_Minimal_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-19.0.1-minimal-200228-linux54.iso.sig"
Download_Minimal_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-19.0.1-minimal-200228-linux54.iso.torrent"
Name = "Cinnamon"
Screenshot = "cinnamon-full.jpg"
edition = "Community"
shortDescription = "For people who look for a traditional desktop with modern technology"
Thumbnail = "cinnamon-full.jpg"
Version = "19.0.2"
date = "2020-03-14T23:24:34UTC"
title = "Cinnamon"
type="download-edition"
weight = "5"
meta_description = "Manjaro cinnamon for people who look for a traditional desktop with modern technology"
meta_keywords = "manjaro cinnamon download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Cinnamon, a desktop based on modern technology that keeps known and proven concepts.

If you are looking for older images check the [Cinnamon](https://osdn.net/projects/manjaro-archive/storage/cinnamon/) archive.

