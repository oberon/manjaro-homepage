+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-architect-19.0-200223-linux54.iso"
Download_x64_Checksum = "a17d9d652abfbc37cc53422f89a061488bb2e856"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-architect-19.0-200223-linux54.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-architect-19.0-200223-linux54.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Architect"
Screenshot = "architect-full.jpg"
edition = "Official"
shortDescription = "Setup and configure Manjaro in every detail using the CLI."
Thumbnail = "architect-full.jpg"
Version = "19.0"
date = "2020-02-25"
title = "Architect"
type="download-edition"
weight = "7"
meta_description = "Manjaro setup and configure Manjaro in every detail using the CLI."
meta_keywords = "manjaro architect download, manjaro download"
+++

This edition comes with a TUI installer.

Manjaro-Architect is a fork of the famous Architect Linux installer by Carl Duff, that has been modified to install Manjaro. It is a netinstaller that downloads the latest available packages, so your system is up to date right after installation, regardless of how old your install media is.

Manjaro-Architect offers total customization on your Manjaro installation:

- choose which Manjaro kernels you want to use, or multiple kernels.
- choose any Manjaro branch (stable, testing or unstable)
- choose your desktop from all current Manjaro editions, regardless of what install media you run the installer from. You can also install the base system only without graphical user interface.
- add any unconfigured desktop environment
- choose the default shell for user (bash, zsh or fish)
- choose your graphics drivers to be installed with mhwd
- choose extra packages to be installed

If you are looking for older images check the [Architect](https://osdn.net/projects/manjaro-archive/storage/architect/) archive.
