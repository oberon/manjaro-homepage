+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-gnome-19.0.1-200228-linux54.iso"
Download_x64_Checksum = "55a8684e186000b8390bbbfb496adf621a32616a"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-gnome-19.0.1-200228-linux54.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-gnome-19.0.1-200228-linux54.iso.torrent"
Download_Minimal_x64 = "https://osdn.net/dl/manjaro/manjaro-gnome-19.0.1-minimal-200228-linux54.iso"
Download_Minimal_x64_Checksum = "808060dc9e02a7f238a3e9456523098d02e984a7"
Download_Minimal_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-gnome-19.0.1-minimal-200228-linux54.iso.sig"
Download_Minimal_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-gnome-19.0.1-minimal-200228-linux54.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "GNOME"
Screenshot = "gnome-full.jpg"
Youtube = "OjyfrFWaLDM"
edition = "Official"
shortDescription = "For people who want a very modern and simple desktop"
Thumbnail = "gnome.jpg"
Version = "19.0.1"
date = "2020-02-28"
title = "Gnome"
type="download-edition"
weigth = "3"
meta_description = "Manjaro gnome for people who want a very modern and simple desktop"
meta_keywords = "manjaro gnome download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with the GNOME 3 desktop that breaks with traditional concepts and allows users to focus on their tasks. Desktop-specific applications are crafted with care and clearly defined by guidelines that make them more consistent to use.

If you are looking for older images check the [GNOME](https://osdn.net/projects/manjaro-archive/storage/gnome/) archive.

