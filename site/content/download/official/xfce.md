+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-xfce-19.0.1-200228-linux54.iso"
Download_x64_Checksum = "a440c3391d7ee05a2c6414b730789a0dc44377dc"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-xfce-19.0.1-200228-linux54.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-xfce-19.0.1-200228-linux54.iso.torrent"
Download_Minimal_x64 = "https://osdn.net/dl/manjaro/manjaro-xfce-19.0.1-minimal-200228-linux54.iso"
Download_Minimal_x64_Checksum = "af50208c7ca57282df70ea9968ec484d9d1b1659"
Download_Minimal_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-xfce-19.0.1-minimal-200228-linux54.iso.sig"
Download_Minimal_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-xfce-19.0.1-minimal-200228-linux54.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "XFCE"
Screenshot = "xfce-full.jpg"
Youtube = "zb_9tc-DiIs"
edition = "Official"
shortDescription = "For people who want a reliable and fast desktop"
Thumbnail = "xfce.jpg"
Version = "19.0.1"
date = "2020-02-28"
title = "XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro xfce for people who want a reliable and fast desktop"
meta_keywords = "manjaro xfce download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with XFCE, a reliable desktop with high configurability.

Xfce is a lightweight desktop environment for UNIX-like operating systems. It aims to be fast and low on system resources, while still being visually appealing and user friendly. Xfce embodies the traditional UNIX philosophy of modularity and re-usability. It consists of a number of components that provide the full functionality one can expect of a modern desktop environment. They are packaged separately and you can pick among the available packages to create the optimal personal working environment.

If you are looking for older images check the [XFCE](https://osdn.net/projects/manjaro-archive/storage/xfce/) archive.


